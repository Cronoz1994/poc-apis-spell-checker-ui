import {Directive, ElementRef, EventEmitter, OnDestroy, OnInit, Output, Renderer2} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {
  HunspellSpellcheckSuggestionResponse,
  HunspellSpellcheckSuggestionsResponse
} from '../services/api-models/hunspell-spellcheck-suggestions.response';
import {HunspellSpellcheckService} from '../services/hunspell-spellcheck.service';

@Directive({
  selector: '[spuSpellCheck]'
})
export class SpuSpellCheckDirective implements OnInit, OnDestroy {

  @Output() showSuggestionsOptions: EventEmitter<any>;

  private _suggestions: HunspellSpellcheckSuggestionResponse [];

  private _keydownSubscription: Subscription;
  private _hunspellSpellcheckSubscription: Subscription;

  constructor(private _hunspellSpellcheckService: HunspellSpellcheckService,
              private _elementRef: ElementRef,
              private _renderer: Renderer2) {
    this._hunspellSpellcheckSubscription = new Subscription();
    this.showSuggestionsOptions = new EventEmitter<any>();
    this._keydownSubscription = new Subscription();
  }

  ngOnInit(): void {
    this._renderer.setAttribute(this._elementRef.nativeElement, 'contenteditable', 'true');
    this._renderer.setAttribute(this._elementRef.nativeElement, 'spellcheck', 'false');

    this._keydownListener();
  }

  ngOnDestroy(): void {
    this._keydownSubscription.unsubscribe();
    this._keydownSubscription = null;

    this._hunspellSpellcheckSubscription.unsubscribe();
    this._hunspellSpellcheckSubscription = null;
  }

  public showSuggestions(event): void {
    event.preventDefault();

    const word: string = event.target.innerText;
    const suggestionsOfWord = this._suggestions.find(suggestion => suggestion.word === word);
    this.showSuggestionsOptions.emit( {event: event, suggestions: suggestionsOfWord});
  }

  private _keydownListener(): void {
    this._keydownSubscription = fromEvent(this._elementRef.nativeElement, 'keyup')
      .pipe(
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe((event) => {
        const text: string = this._elementRef.nativeElement.innerText;

        const words: string = text.replace(/\s/g, ',');

        if (words && event['code'] !== 'Backspace' && event['code'] !== 'Space') {
          this._hunspellSpellcheckSubscription.unsubscribe();

          this._hunspellSpellcheckSubscription = this._hunspellSpellcheckService.getSuggestions(words)
            .subscribe((suggestionsResponse: HunspellSpellcheckSuggestionsResponse) => {
              this._suggestions = suggestionsResponse.suggestions;

              this._validateText(this._suggestions);
            });
        }
      });
  }

  private _validateText(suggestions: HunspellSpellcheckSuggestionResponse []): void {
    const childNodes: any [] = this._elementRef.nativeElement.childNodes;
    console.log(childNodes);
    console.log(this._elementRef);
    childNodes.forEach((node: any) => {
      const auxNodes: any [] = [];
      const words: string [] = node.textContent.replace(/\s/g, ',').split(',');

      words.forEach((word: string) => {
        const wordInSuggestions = suggestions.find((suggestion) => suggestion.word === word);

        if (wordInSuggestions && wordInSuggestions.misspelled) {
          const wrapperForMisspelledWord = this._renderer.createElement('span');
          this._renderer.setAttribute(wrapperForMisspelledWord, 'class', 'scu-content-box__word--red-wiggle');
          this._renderer.setAttribute(wrapperForMisspelledWord, 'oncontextmenu', 'return false');
          wrapperForMisspelledWord.innerText = word;

          auxNodes.push(wrapperForMisspelledWord);
          auxNodes.push(this._renderer.createText(' '));
        } else {
          auxNodes.push(this._renderer.createText(word));
        }
      });

      auxNodes.forEach((auxNode) => {
        this._renderer.insertBefore(this._renderer.parentNode(node), auxNode, node);
      });

      this._renderer.removeChild(this._renderer.parentNode(node), node);

      if (this._elementRef.nativeElement.querySelectorAll('span')) {
        Array.from(this._elementRef.nativeElement.querySelectorAll('span'))
          .forEach((selector: HTMLSpanElement) => {
            selector.addEventListener('contextmenu', this.showSuggestions.bind(this));
          });
      }
    });

  }

}
