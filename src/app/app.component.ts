import {Component, ElementRef, HostListener, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('suggestionMenu') suggestionMenu: ElementRef<any>;

  @HostListener('document:click', ['$event']) public onDocumentElementClick: (event: any) => void;

  public title = 'Base Spell Checker';

  public showSuggestionsMenu: boolean;
  public suggestions: string;

  constructor(private _renderer: Renderer2) {
    this.onDocumentElementClick = this._onDocumentClick;
    this.showSuggestionsMenu = false;
  }

  public showSuggestionsOptions({event, suggestions}): void {
    console.log(this.suggestionMenu);
    this._renderer.setStyle(this.suggestionMenu.nativeElement, 'top', `${event.y}px`);
    this._renderer.setStyle(this.suggestionMenu.nativeElement, 'left', `${event.x}px`);

    this.suggestions = suggestions.suggestions;

    this._renderer.setStyle(this.suggestionMenu.nativeElement, 'visibility', 'visible');

    this.showSuggestionsMenu = true;
  }

  private _onDocumentClick(event: any): void {
    if (this.showSuggestionsMenu && !event.target.closest('.scu-content-box__suggestions')) {
      this.showSuggestionsMenu = false;
      this._renderer.setStyle(this.suggestionMenu.nativeElement, 'visibility', 'hidden');
    }
  }

}
