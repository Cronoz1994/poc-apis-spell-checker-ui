
export interface HunspellSpellcheckSuggestionsResponse {
  suggestions: HunspellSpellcheckSuggestionResponse [];
}

export interface HunspellSpellcheckSuggestionResponse {
  word: string;
  misspelled: boolean;
  suggestions: string [];
}
