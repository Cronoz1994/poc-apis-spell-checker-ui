import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HunspellSpellcheckSuggestionsResponse} from './api-models/hunspell-spellcheck-suggestions.response';

@Injectable({
  providedIn: 'root'
})
export class HunspellSpellcheckService {

  private readonly _BASE_PATH = 'http://localhost:8080/spellcheck/words';

  constructor(private http: HttpClient) {
  }

  public getSuggestions(words: string): Observable<HunspellSpellcheckSuggestionsResponse> {
    const path = `${this._BASE_PATH}/${words}`;
    return this.http.post<HunspellSpellcheckSuggestionsResponse>(path, null);
  }

}
